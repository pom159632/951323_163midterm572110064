﻿public enum ItemType
{
    TIMER_UP,
    TIMER_DOWN,
    SCORE,
    OBSTACLE
}
