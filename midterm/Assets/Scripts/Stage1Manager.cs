﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Stage1Manager : MonoBehaviour
{
    [SerializeField] private float timer = 30f;
    private float score=0;
    public Text Tscore;
    public Text Ttimer;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        Ttimer.text = timer.ToString();
        score = timer * 1000;
        Tscore.text = score.ToString();


        if (timer < 0)
        {
            SceneManager.LoadScene("Scene_MainMenu");
        }
        
    }
   
}
