﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


using UnityEngine.SceneManagement;
public class MainMenuControl : MonoBehaviour
{
   //Declarations section
 [SerializeField] Button _startButton;
 [SerializeField] Button _optionsButton;
    [SerializeField] Button _creditsButton;

    [SerializeField] Button _exitButton;


    float nowi =0;
    float idleTime = 5; 
 //Inside Start() method
 void Start () {
 _startButton.onClick.AddListener (
 delegate{StartButtonClick(_startButton);});
 _optionsButton.onClick.AddListener (
 delegate{OptionsButtonClick(_optionsButton);});
        _creditsButton.onClick.AddListener(
 delegate { CreditsButtonClick(_optionsButton); });
        _exitButton.onClick.AddListener (
 delegate{ExitButtonClick(_exitButton);});
 }

 // Update is called once per frame
 void Update () {
        if (Input.anyKey)
        {
            nowi = 0;
        }
        else
        { nowi += Time.deltaTime; }

        if (nowi>idleTime)
        {
            SceneManager.LoadScene("Scene_Splash");
        }
    }



 public void StartButtonClick(Button button) {
 SceneManager.LoadScene("Scene_StageSelect");
 }

 public void OptionsButtonClick(Button button) {
 if (!GameApplicationManager.Instance.IsOptionMenuActive)
 {
 SceneManager.LoadScene("Scene_Options", LoadSceneMode.Additive);
 GameApplicationManager.Instance.IsOptionMenuActive = true;
 }
 }

    public void CreditsButtonClick(Button button)
    {
        SceneManager.LoadScene("Scene_Credit");
    }

    public void ExitButtonClick(Button button) {
 Application.Quit();
 }

  
 }