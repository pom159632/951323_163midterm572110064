﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player2 : MonoBehaviour
{
    [SerializeField] private float scorebase;
    [SerializeField] private float timer = 20;
    public Text timerT;
    public Text scoreT;
    // Start is called before the first frame update
    void Start()
    {
        scorebase = timer * 1000;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        scorebase -= (Time.deltaTime * 1000);
        if (timer < 0)
        {
            SceneManager.LoadScene("Scene_MainMenu");
        }
        timerT.text = timer.ToString();
        scoreT.text = scorebase.ToString();


    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Goal")
        {
            SceneManager.LoadScene("Scene_MainMenu");
        }
        else
        {
            SceneManager.LoadScene("Scene_MainMenu");
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Item")
        {
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "Obstacle")
        {
            Destroy(collision.gameObject, 0.5f);
        }

        ItemTypeComponent itc = collision.gameObject.GetComponent<ItemTypeComponent>();

        if (itc != null)
        {
            switch (itc.Type)
            {
                case ItemType.SCORE:
                    scorebase += 500;
                    break;
                case ItemType.OBSTACLE:
                    break;
                case ItemType.TIMER_UP:
                    timer += 3;
                    break;
                case ItemType.TIMER_DOWN:
                    timer -= 5;
                    break;
                                                                             
            }

        }
    }
}

